public class Main {

    public static void main(String[] args) {
        ChatStationImpl chat = new ChatStationImpl();

        chat.add("Amy");
        chat.add("Bob");
        chat.add("Chris");
        chat.add("Daniel");
        chat.sendMessage("Amy", "Bob", "Hey there!");
        chat.sendMessage("Daniel", "Chris", "I hate you!");
    }
}
