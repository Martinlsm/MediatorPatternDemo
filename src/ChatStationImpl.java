import java.util.HashMap;

public class ChatStationImpl implements ChatStation {

    private HashMap<String, Chatter> chatters = new HashMap<>();

    public void add(String name) {
        chatters.put(name, new ChatterPerson(name, this));
    }

    @Override
    public void sendMessage(String from, String to, String message) {
        chatters.get(to).receiveMessage(chatters.get(from), message);
    }
}
