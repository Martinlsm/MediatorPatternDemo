public class ChatterPerson implements Chatter {

    private String name;
    private ChatStation chat;

    public ChatterPerson(String name, ChatStation chat) {
        this.chat = chat;
        this.name = name;
    }

    @Override
    public void text(String to, String message) {
        chat.sendMessage(name, to, message);
    }

    @Override
    public void receiveMessage(Chatter from, String message) {
        System.out.println(this + " receives a message from " + from);
        System.out.println("Message: ");
        System.out.println(message + "\n");
    }

    public String toString() {
        return name;
    }
}
