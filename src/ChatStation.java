public interface ChatStation {

    void sendMessage(String from, String to, String message);

}
