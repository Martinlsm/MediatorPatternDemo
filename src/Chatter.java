public interface Chatter {

    void text(String to, String message);

    void receiveMessage(Chatter from, String message);
}
